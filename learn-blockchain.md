# features

* decentralized
* secure
* anonymous
* traceable
    * claim

# what to do

* read Renlord's [CABRA](https://cdecker.github.io/btcresearch/)
    * incumbent org
        * Distributed Ledgers
        * bank
        * _the blockchain, a hash table with encrypted content shared by a user,
        a Turing complete relationship system to control the the maximum number
        of shares performed by user’s circle members and a local personal
        certificate authority that manages the user’s circles and encrypts data
        to be shared before it is broadcasted to the network_
    * disrupt the world of social networking
        * user centric blockchain supported social media network
        * control, trace and claim ownership of every piece of content they
        share
        * content distribution network
        * what is Ushare
            * the blockchain
            * a hash table with encrypted content shared by a
            user
            * a Turing complete relationship system to control the the
            maximum number of shares performed by user’s circle members
            * a local personal certificate authority that manages the user’s circles
            and encrypts data to be shared before it is broadcasted to the
            network
    * Pervasive Decentralisation Of Digital Infrastructures
        * decentralized grids for computation and storage to global financial
        services
    + smart city
        + physical
        + social
        + business
    + Disruptive Potential in Voting Industry
        + 
